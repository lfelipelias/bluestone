# README #

This is a service API created using Quarkus and with integration to https://holidayapi.com/



### The service can be accessed using the following :

- URL: http://localhost:8080/bluestone/holiday?country1=IRELAND&country2=BRAZIL&holidayDate=2021-01-01

Parameters:

- country1 = POLAND (the first country to check the holiday)

- country2 = BRAZIL (the second country to check the holiday)

- holidayDate = 2021-01-01 (the date in format yyyy-MM-dd used to query the upcoming holiday in both countries)

Responses examples:

- success case : 
{
    "date": "2021-01-01",
    "name1": "Epiphany",
    "name2": "Shrove Monday"
	"country1": "IRELAND",
    "country2": "BRAZIL"
}



- validation error:
[
	"Field country1 is mandatory"
]
	
	
	
### Project structure ###

	.
	├── config                  # Relevant config for the project
	├── controller              # The entry point for accessing services
	├── service                 # Services responsible for the business flow of the application
	├── utils                   # Utility classes
	└── Main.Java				# The main class used to start the project
	

### Evidences & Docs ###

In the folder Documentation the following files are present:

- Postman evidence.pdf : Contains prints of the success, error and validation flow

- Bluestone.postman_collection.json : Is the postman collection to call the service


### Installing and running it using Docker and pushing to dockerhub###
To install and run the jar use the following steps:

- Create jar using the command : 

mvn install in the project root

- Build docker image locally using the command to build build image: 

docker build -t holiday:3.0 .


- Run mongodb image using the command: 

docker run -d -p 27017:27017 --network holiday-network --name example-mongo  -v mongo-data:/data/db  -e MONGODB_INITDB_ROOT_USERNAME=username  -e MONGODB_INITDB_ROOT_PASSWORD=password -e MONGO_INITDB_DATABASE=holiday  mongo:latest


- Run the project image using the command: 

docker run --network holiday-network  --name holiday -p 8080:8080  holiday:3.0