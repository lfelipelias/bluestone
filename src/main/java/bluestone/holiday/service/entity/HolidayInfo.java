package bluestone.holiday.service.entity;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MongoEntity(collection="HolidayInfo")
public class HolidayInfo extends PanacheMongoEntity  {
	private String date;
	private String name;
	private String holidayName;
	private String country;
}