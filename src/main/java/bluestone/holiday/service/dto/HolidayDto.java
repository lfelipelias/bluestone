package bluestone.holiday.service.dto;

import java.io.Serializable;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RegisterForReflection
public class HolidayDto implements Serializable {

	private static final long serialVersionUID = 832619399636444687L;
	
	private String date;
	private String name1;
	private String name2;
	private String country1;
	private String country2;
}