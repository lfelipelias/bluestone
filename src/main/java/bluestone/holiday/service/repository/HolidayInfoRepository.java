package bluestone.holiday.service.repository;

import javax.enterprise.context.ApplicationScoped;

import bluestone.holiday.service.entity.HolidayInfo;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

@ApplicationScoped
public class HolidayInfoRepository implements PanacheMongoRepository<HolidayInfo>  {
	/**
	 * Find Holiday by:
	 * @param country
	 * @param date
	 * @return
	 */
	public HolidayInfo findByCountryAndDate(String country, String date) {
		
		return HolidayInfo.find("country = ?1 and date = ? 2", country, date).firstResult();
		
	}
	
}