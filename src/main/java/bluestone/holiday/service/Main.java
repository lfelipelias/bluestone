package bluestone.holiday.service;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain(name = "BluestoneHolidayService")
public class Main {

	/**
	 * 
	 * @param args
	 */
    public static void main(String ... args) {
        System.out.println("Running BluestoneHolidayService");
        Quarkus.run(args); 
    }
    
}