package bluestone.holiday.service.controller.validator;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;

import com.github.agogs.holidayapi.model.QueryParams.Country;

import bluestone.holiday.service.utils.DateUtils;

@ApplicationScoped
public class HolidayValidator extends AbstractHolidayValidator {

	/**
	 * Method used to validate mandatory fields
	 * 
	 * @param country1
	 * @param country2
	 * @param holidayDate
	 * @return
	 */
	public List<String> validateInputs(Country country1, Country country2, String holidayDate) {
		List<String> errors = new ArrayList<>();

		if (country1 == null) {
			errors.add(MANDATORY_FIELD_MSG.replace("{0}", "country1"));
		}

		if (country2 == null) {
			errors.add(MANDATORY_FIELD_MSG.replace("{0}", "country1"));
		}

		if (StringUtils.isBlank(holidayDate)) {
			errors.add(MANDATORY_FIELD_MSG.replace("{0}", "holidayDate"));

		} else if (DateUtils.isValidDateFmt(holidayDate, DateUtils.DEFAULT_FMT) == false) {
			errors.add(FIELD_INVALID.replace("{0}", "holidayDate"));
		}

		return errors;
	}

}
