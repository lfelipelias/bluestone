package bluestone.holiday.service.controller.validator;

public abstract class AbstractHolidayValidator {

	public static final String MANDATORY_FIELD_MSG = "Field {0} is mandatory";
	public static final String FIELD_INVALID = "Field {0} is invalid";

}
