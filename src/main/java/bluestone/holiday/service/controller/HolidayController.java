package bluestone.holiday.service.controller;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.github.agogs.holidayapi.model.QueryParams.Country;

import bluestone.holiday.service.controller.validator.HolidayValidator;
import bluestone.holiday.service.service.HolidayService;

/**
 * The Rest API for the holiday service
 *  
 * @author lfelipelias
 *
 */
@Path("/holiday")
public class HolidayController {

	@Inject
	HolidayService holidayService;

	@Inject
	HolidayValidator holidayValidator;

	/**
	 * The GET entry for the holiday service
	 * 
	 * @param country1 1st country used as filter 
	 * @param country2 2nd country used as filter
	 * @param holidayDate  date used as filter
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHoliday(@QueryParam("country1") Country country1, @QueryParam("country2") Country country2, @QueryParam("holidayDate") String holidayDate) {
		
		
		final List<String> errors = holidayValidator.validateInputs(country1, country2, holidayDate);
		
		//Case mandatory fields are not filled return a bad request
		if (errors !=null && errors.size() > 0) {
			return Response.ok(errors).status(Response.Status.BAD_REQUEST).build();
		} else {
			return holidayService.findHolidays(country1, country2, holidayDate);
		}
		
	}

}
