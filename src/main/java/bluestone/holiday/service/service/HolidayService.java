package bluestone.holiday.service.service;

import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.github.agogs.holidayapi.api.APIConsumer;
import com.github.agogs.holidayapi.api.impl.HolidayAPIConsumer;
import com.github.agogs.holidayapi.model.Holiday;
import com.github.agogs.holidayapi.model.HolidayAPIResponse;
import com.github.agogs.holidayapi.model.QueryParams;
import com.github.agogs.holidayapi.model.QueryParams.Country;

import bluestone.holiday.service.dto.HolidayDto;
import bluestone.holiday.service.entity.HolidayInfo;
import bluestone.holiday.service.repository.HolidayInfoRepository;
import bluestone.holiday.service.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * The holiday service with connection to the HolidayAPI service
 * 
 * @author lfelipelias
 *
 */
@ApplicationScoped
@Slf4j
public class HolidayService {

	@ConfigProperty(name = "apiholiday.url")
	String apiUrl;
	
	@ConfigProperty(name = "apiholiday.key")
	String apiKey;
	
	@Inject
	HolidayInfoRepository repository;
	
	/**
	 * 
	 * @param country1
	 * @param country2
	 * @param holidayDate
	 * 
	 * @return HolidayDto containing data of the holidays or error in case of exception 
	 */
	public Response findHolidays(Country country1, Country country2, String holidayDate) {
		log.info(">>>>> HolidayService::findHolidays({} {} {}): START", country1, country2, holidayDate);

		final HolidayDto retorno = new HolidayDto();
		
		retorno.setDate(holidayDate);
		
		
		try {
			
			final LocalDate date = DateUtils.stringToDate(holidayDate, DateUtils.DEFAULT_FMT);
			final Holiday holiday1 = findHoliday(country1, date);
			final Holiday holiday2 = findHoliday(country2, date);	
			
			if(holiday1 != null) {
				retorno.setCountry1(country1.toString());
				retorno.setName1(holiday1.getName());
			};
			
			if(holiday2 != null) {
				retorno.setCountry2(country2.toString());
				retorno.setName2(holiday2.getName());
			}
			
			
			
		} catch (Exception e) {
			
			//Case of exception deliver friendly output to the user
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		
		log.info(">>>>> HolidayService::findHolidays({} {} {}): START", country1, country2, holidayDate);
		return Response.ok(retorno).status(Response.Status.OK).build();
	}

	/**
	 * Find HolidayInfo in database
	 * @param country
	 * @param date
	 * @return
	 */
	private Holiday findHolidayDb(Country country, LocalDate date) {
		log.info(">>>>> HolidayService::findHolidayDb({} {}): START", country, date);
		final HolidayInfo found = repository.findByCountryAndDate(country.toString(), date.toString());
		if(found != null) {
			log.info("holiday {} found in DB", found.getHolidayName());
			
			final Holiday holiday = new Holiday();
			holiday.setName(found.getHolidayName());
			return holiday;
		} 
		log.info("holiday not found in DB");
		return null;
	}
	/**
	 * Connection with the HolidayAPI
	 * 
	 * @param country
	 * @param date
	 * @return
	 * @throws Exception
	 */
	private Holiday findHoliday(Country country, LocalDate date) throws Exception {
		log.info(">>>>> HolidayService::findHoliday({} {}): START", country, date);
		
		//Check first if the HolidayInfo is already persisted to avoid external call
		Holiday holidayFound = findHolidayDb(country, date);
		if(holidayFound != null) {
			return holidayFound;	
		}
		
		//HolidayInfo not present in the database, so calling external API
		final APIConsumer consumer = new HolidayAPIConsumer(apiUrl);

		final QueryParams params = new QueryParams()
				.key(apiKey)
				.day(date.getDayOfMonth())
				.month(date.getMonthValue())
				.upcoming(Boolean.TRUE)
				.country(country)
				.year(date.getYear())
				.pretty(true);

		try {

			log.info("calling HolidayApi with params: {} {}", country, date);
			
			final HolidayAPIResponse response = consumer.getHolidays(params);

			int status = response.getStatus();

			if (status != Response.Status.OK.getStatusCode()) {

				throw new IllegalArgumentException(response.getError());
				
			} else {

				final List<Holiday> holidays = response.getHolidays();
				if (holidays == null || holidays.size() == 0) {
					log.info("holiday not found in HolidayApi");
					return null;
				} else {
					log.info("holiday {} returned from HolidayApi", holidays.get(0).getName());

					//Persist in database for future calls
					final HolidayInfo info = new HolidayInfo();
					info.setDate(date.toString());
					info.setCountry(country.toString());
					info.setHolidayName(holidays.get(0).getName());
					
					repository.persist(info);
					
					return holidays.get(0);
					
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

}
