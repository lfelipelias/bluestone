package bluestone.holiday.service.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

	public static final String DEFAULT_FMT = "yyyy-MM-dd";

	public static Boolean isValidDateFmt(String date, String fmt) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);
		try {
			LocalDate.parse(date, formatter);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	public static LocalDate stringToDate(String date, String fmt) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);
		return LocalDate.parse(date, formatter);

	}
}
