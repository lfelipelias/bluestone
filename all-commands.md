### Installing and running it using Docker and pushing to dockerhub ###

- Create jar using the command : 

	mvn install in the project root

- Build docker image locally using the command to build build image: 

	docker build -t holiday:3.0 .


- Run mongodb image using the command: 

	docker run -d -p 27017:27017 --network holiday-network --name example-mongo  -v mongo-data:/data/db  -e MONGODB_INITDB_ROOT_USERNAME=username  -e MONGODB_INITDB_ROOT_PASSWORD=password -e MONGO_INITDB_DATABASE=holiday  mongo:latest


- Run the project image using the command: 

	docker run --network holiday-network  --name holiday -p 8080:8080  holiday:3.0
	
- Tag image and push to dockerhub :

	docker tag <id_image_built_step_above> lfelipelias/holidayservice:3.0
	docker push lfelipelias/holidayservice
	

### Deploying and running in Kubernetes ###
    kubectl apply -f mongo-configmap.yaml
    kubectl apply -f mongo-secret.yaml 
    kubectl apply -f mongo-deployment.yaml
    kubectl apply -f mongo-express-deployment.yaml
    kubectl apply -f mongo-express-ingress.yaml
    kubectl apply -f holiday-deployment.yaml
    kubectl apply -f holiday-ingress.yaml
    
    //get ip address from ingress
    kubectl get ingress
    
    //edit /etc/hosts and map ingress ip to the URL below
    vim /etc/hosts
    
    <ip_from_holiday-ingress>    holiday.com
	<ip_from_mongo_ingress>      mongoexpress.com
	
	
	//call api service using the url:
	http://holiday.com/bluestone/holiday?country1=IRELAND&country2=BRAZIL&holidayDate=2021-01-01
	
	//access mongodb-express to see the database holiday created
	http://mongoexpress.com/
    
