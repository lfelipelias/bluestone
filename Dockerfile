FROM openjdk:17-alpine
RUN mkdir app

ARG JAR_FILE
ADD target/holiday-service-1.0.0-runner.jar /app/holiday-service-1.0.0-runner.jar

ENV DB_URI 'mongodb://username:password@example-mongo/holiday'
ENV HOLIDAY_API_KEY 'b3ee5d39-5d85-4a10-b5a6-ee86171862c6'

WORKDIR /app
ENTRYPOINT java -jar holiday-service-1.0.0-runner.jar

EXPOSE 80/tcp